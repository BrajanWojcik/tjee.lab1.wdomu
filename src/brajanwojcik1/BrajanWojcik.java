/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brajanwojcik1;

import java.io.BufferedReader; //
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader; //
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

/**
 *
 * @author student
 */
public class BrajanWojcik {

    public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String JDBC_URL = "jdbc:derby:/home/student/NetBeansProjects/BrajanWojcik/db/BrajanWojcik_db1";
    public static final String QUERY = "select * from app.jobs";
    
    private static java.sql.Connection conn;
        private BrajanWojcik() { }
        public static boolean Connect() throws ClassNotFoundException, SQLException {
        conn = DriverManager.getConnection(JDBC_URL);
        if (conn == null) {
        return false;
        } else {
        return true;
        }
        }
        public static boolean Disconnect() throws SQLException {
        if (conn == null) {
        return false;
        } else {
        conn.close();
        return true;
        }
        }
        
        public static void doPliku() throws IOException, SQLException {
            Statement stat = conn.createStatement();
            ResultSet rs = stat.executeQuery(QUERY);
            ResultSetMetaData rsmd = rs.getMetaData();
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //
            FileWriter fw = new FileWriter("/home/student/NetBeansProjects/BrajanWojcik/m.txt"); //
            String wiersz = new String();
            int colCount = rsmd.getColumnCount();
            for (int i = 1; i <= colCount; i++) {
                wiersz = wiersz.concat(rsmd.getColumnName(i) + " \t| ");
                //fw.write(wiersz);
            }
            fw.write(wiersz);
        }
       
        public static String getData() throws SQLException {
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(QUERY);
        ResultSetMetaData rsmd = rs.getMetaData();
        String wiersz = new String();
        int colCount = rsmd.getColumnCount();
        for (int i = 1; i <= colCount; i++) {
        wiersz = wiersz.concat(rsmd.getColumnName(i) + " \t| ");
        }
        wiersz = wiersz.concat("\r\n");
        for(int i=0; i<=55; i++)
        wiersz = wiersz.concat("----");
        wiersz = wiersz.concat("\r\n");
        while (rs.next()) {
        System.out.println("");
        for (int i = 1; i <= colCount; i++) {
        wiersz = wiersz.concat(rs.getString(i) + " \t| ");
        }
        wiersz = wiersz.concat("\r\n");
        }
        if (stat != null) { stat.close(); } return wiersz; }
        
        
    /*public static void main(String[] args) throws ClassNotFoundException, SQLException {
        
        java.sql.Connection conn=DriverManager.getConnection(JDBC_URL);
        System.out.println("Połączono");
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(QUERY);
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        for(int i=1; i<=colCount; i++)
            System.out.print(rsmd.getColumnName(i)+ " | ");
        while (rs.next()) {
            System.out.println("");
            for(int i=1; i<=colCount; i++)
                System.out.print(rs.getString(i)+ " \t| ");
        }
        if (stat!=null) stat.close();
        if (conn!=null) conn.close();
        System.out.println("\nRozłączono");
    }*/ 
}
